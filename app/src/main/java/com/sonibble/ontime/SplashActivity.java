package com.sonibble.ontime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sonibble.ontime.service.AccountService;

public class SplashActivity extends AppCompatActivity {
    // define the service
    private final AccountService accountService = new AccountService();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // check the user signed
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // validate user sign
                validateUserSign();
            }
        }, 2000);
    }


    /**
     * ## validateUserSign
     *
     * validate the user and define the passed activity
     * when user found or not
     */
    private void validateUserSign(){
        final Boolean isSignIn = accountService.checkUserSign(getApplicationContext());
        if(isSignIn){
            // bring the user into the dashboard
            Intent dashboardIntent = new Intent(SplashActivity.this, DashboardActivity.class);
            startActivity(dashboardIntent);
            finish();
        }else{
            // bring the user to sign first
            Intent logInIntent = new Intent(SplashActivity.this, LogInActivity.class);
            startActivity(logInIntent);
            finish();
        }
    }
}