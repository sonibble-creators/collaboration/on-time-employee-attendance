package com.sonibble.ontime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sonibble.ontime.model.User;
import com.sonibble.ontime.service.AccountService;

public class LogInActivity extends AppCompatActivity {
    private EditText inputEmail;
    private EditText inputPassword;
    private Button logInButton;
    private TextView createAccountLink;

    private final AccountService accountService = new AccountService();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        // define the widget variables
        inputEmail = findViewById(R.id.inputEmail);
        inputPassword = findViewById(R.id.inputPassword);
        logInButton = findViewById(R.id.logInButton);
        createAccountLink = findViewById(R.id.createAccountLink);


        // when the createAccountTapped
        createAccountLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bringToCreateAccount();
            }
        });


        // when the sign button clicked
        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // start to sign the user
                signInUser();
            }
        });
    }

    /**
     * ## signInUser
     *
     * sign the user in
     * using email and password
     */
    private void signInUser(){
        // change the button state
        logInButton.setText("Signing...");

        // first we need to get all data from the form
        final String email = inputEmail.getText().toString().trim();
        final String password = inputPassword.getText().toString().trim();

        // start check the user
        // are the user already found or not
        final Boolean isSigned = accountService.signIn(email, password, getApplicationContext());

        // bring back the button state
        logInButton.setText("Explore Now");

        // start validate
        if(isSigned){
            // bring the user to dashboard
            final Intent dashboardIntent = new Intent(LogInActivity.this, DashboardActivity.class);
            startActivity(dashboardIntent);
            finish();
        }
    }


    /**
     * ## bringToCreateAccount
     *
     * bring the user to create a new account of user
     */
    private void bringToCreateAccount(){
        // define the intent
        Intent createAccountIntent = new Intent(LogInActivity.this, CreateAccountActivity.class);
        startActivity(createAccountIntent);
        finish();
    }
}