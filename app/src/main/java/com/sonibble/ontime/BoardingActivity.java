package com.sonibble.ontime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BoardingActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boarding);

    }


    // when the get Started tapped
    public void getStarted(View view){
        // when get started,
        // we're going to sign in
        Intent signInIntent = new Intent(BoardingActivity.this, LogInActivity.class);
        startActivity(signInIntent);
        finish();
    }



}