package com.sonibble.ontime.utils;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.sonibble.ontime.model.History;
import com.sonibble.ontime.model.User;
import com.sonibble.ontime.repository.HistoryRepo;
import com.sonibble.ontime.repository.UserRepo;

@Database(entities = {User.class, History.class}, version = 4, exportSchema = false)
@TypeConverters({DatabaseConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;
    private static final String DB_NAME = "awesome_app_db";

    // get the database instance
    // base on the current context of applications
    public static synchronized AppDatabase getInstance(Context context){
        if(instance == null){
            // define the new instance of the database
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DB_NAME).fallbackToDestructiveMigration().allowMainThreadQueries().build();
        }

        // now return the local database
        return instance;
    }

    // define all user repositories
    public abstract UserRepo userRepo();
    public abstract HistoryRepo historyRepo();
}
