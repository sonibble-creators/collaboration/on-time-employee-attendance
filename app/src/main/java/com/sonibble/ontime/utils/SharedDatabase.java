package com.sonibble.ontime.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedDatabase {
    private static SharedPreferences instance;
    private static final String SHARED_PREF_NAME = "local_prefs";

    // get the current instance of
    // shared preferences
    public static synchronized SharedPreferences getInstance(Context context){
        if(instance == null){
            instance = context.getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }

        // return the current instance
        return instance;
    }
}
