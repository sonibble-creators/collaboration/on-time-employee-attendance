package com.sonibble.ontime.utils;

public class CollectionKey {
    public static final String EMAIL_PREFS_KEY = "email";
    public static final String PASSWORD_PREFS_KEY = "password";
}
