package com.sonibble.ontime.repository;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.sonibble.ontime.model.History;

import java.util.Date;
import java.util.List;

@Dao
public interface HistoryRepo {
    @Query("SELECT * FROM history")
    LiveData<List<History>> findAll();

    @Query("SELECT * FROM history")
    List<History> findAllHistories();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(History... histories);

    @Query("SELECT * FROM history WHERE current_date = :date LIMIT 1")
    History findByCurrentDate(Date date);

    @Query("SELECT * FROM history WHERE current_date <= date('now') ORDER BY current_date ASC LIMIT 1")
    History findTheNewDailyHistory();

    @Delete
    void delete(History history);

    @Update
    void updateAll(History... histories);
}
