package com.sonibble.ontime.repository;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.sonibble.ontime.model.User;

import java.util.List;

@Dao
public interface UserRepo {
    @Query("SELECT * FROM user")
    LiveData<List<User>> findAll();

    @Query("SELECT * FROM user")
    List<User> findAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(User... users);

    @Query("SELECT * FROM user WHERE email = :email AND password = :password LIMIT 1")
    User findUserByEmailAndPassword(String email, String password);

    @Query("SELECT * FROM user WHERE email = :email LIMIT 1")
    User findUserByEmail(String email);

    @Delete
    void delete(User user);

    @Update
    void updateAll(User... users);
}
