package com.sonibble.ontime.service;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class StorageService {
    private final String IMAGE_DIR = "images";

    /**
     * ## saveImageToInternalStorage
     *
     * save the image into the internal storage
     *
     * @param bitmapImage bitmap image
     * @param context context of application running
     * @param fileName filename including the file extentions
     * @return String
     */
    public String saveImageToInternalStorage(Bitmap bitmapImage, Context context, String fileName){

        // define the context
        // and the directory inside the data of app
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File directory = cw.getDir(IMAGE_DIR, Context.MODE_PRIVATE);

        // create the file with directory
        File filePath =new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(fos != null) fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // return the absolute path
        // and so we can load it
        return directory.getAbsolutePath() + "/" + fileName;
    }

    /**
     * load image from storage
     * @param path path of image located
     * @return Bitmap
     */
    public Bitmap loadImageFromStorage(String path)
    {
        try {
            File file = new File(path);
            return BitmapFactory.decodeStream(new FileInputStream(file));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // return null when not found
        return null;

    }

}
