package com.sonibble.ontime.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.sonibble.ontime.model.User;
import com.sonibble.ontime.utils.AppDatabase;
import com.sonibble.ontime.utils.CollectionKey;
import com.sonibble.ontime.utils.SharedDatabase;

import java.util.List;

public class AccountService {

    /**
     * ## checkUserSign
     *
     * check the user is signed or not
     * will return the boolean value of status of user sign
     * @param context context of application activity
     * @return Boolean
     */
    public Boolean checkUserSign(Context context){
        // grab the shared instance
        // when the user created or signed they save some value in the locals
        final SharedPreferences prefs =SharedDatabase.getInstance(context);

        // now get some data from local
        final String email = prefs.getString(CollectionKey.EMAIL_PREFS_KEY, null);
        final String password = prefs.getString(CollectionKey.PASSWORD_PREFS_KEY, null);

        // now check if the user already sign or not
        return email != null && password != null;
    }

    /**
     * ## signIn
     *
     * sign the user
     * user the email and password
     *
     * @param email email of the user
     * @param password password of the user
     * @param context context application activity
     * @return Boolean
     */
    public Boolean signIn(String email, String password, Context context){
        // check the data from the repository of user
        final User userSign = AppDatabase.getInstance(context).userRepo().findUserByEmailAndPassword(email, password);
        if(userSign != null && userSign.email.equals(email) && userSign.password.equals(password)){
            // save the data locally too
            final SharedPreferences prefs = SharedDatabase.getInstance(context);
            final SharedPreferences.Editor editor = prefs.edit();
            editor.putString(CollectionKey.EMAIL_PREFS_KEY, email);
            editor.putString(CollectionKey.PASSWORD_PREFS_KEY, password);
            editor.apply();

            return true;
        }else{
            return false;
        }
    }

    public void logOut(Context context){
        // remove all data locally
        final SharedPreferences prefs = SharedDatabase.getInstance(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CollectionKey.EMAIL_PREFS_KEY, null);
        editor.putString(CollectionKey.PASSWORD_PREFS_KEY, null);
        editor.apply();
    }

    /**
     * ## createAccount
     *
     * create a new account for
     * user
     * @param fullName full name of user
     * @param email email of user
     * @param password password of user
     * @param context application context activity
     */
    public void createAccount(String fullName, String email, String password, Context context){
        // now define the user model
        User userInput = new User();
        userInput.email = email;
        userInput.password = password;
        userInput.fullName = fullName;
        userInput.username = email.split("@")[0];

        // start to save in database
        AppDatabase.getInstance(context).userRepo().insertAll(userInput);

        // also save in locally
        // save the data locally too
        final SharedPreferences prefs = SharedDatabase.getInstance(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CollectionKey.EMAIL_PREFS_KEY, email);
        editor.putString(CollectionKey.PASSWORD_PREFS_KEY, password);
        editor.apply();
    }

    /**
     * ## currentUser
     *
     * the current user that want to get the the complete profile
     *
     *
     * @param context context of application activity
     * @return user
     */
    public User currentUser(Context context){
        // grab the shared instance
        // when the user created or signed they save some value in the locals
        final SharedPreferences prefs = SharedDatabase.getInstance(context);

        // now get some data from local
        final String email = prefs.getString(CollectionKey.EMAIL_PREFS_KEY, null);

        // find the same user with the same email
        return AppDatabase.getInstance(context).userRepo().findUserByEmail(email);
    }

    /**
     * ## updateUser
     *
     * update the current user
     * @param userInput the new user input
     * @param context context of application for activity
     */
    public void updateUser(User userInput, Context context){
        AppDatabase.getInstance(context).userRepo().updateAll(userInput);
    }
}
