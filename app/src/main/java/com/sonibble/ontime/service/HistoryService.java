package com.sonibble.ontime.service;

import android.content.Context;
import android.util.Log;

import com.sonibble.ontime.model.History;
import com.sonibble.ontime.utils.AppDatabase;

import java.util.Date;
import java.util.List;

public class HistoryService {


    /**
     * ## initHistoryDaily
     *
     * find and define the daily history
     * for company to absence
     *
     * @param context application context of activity
     * @return History
     */
    public History initHistoryDaily(Context context){
        // first we're going to check the latest history
        // based on today date
        History currentHistory = AppDatabase.getInstance(context).historyRepo().findTheNewDailyHistory();
        List<History> histories = AppDatabase.getInstance(context).historyRepo().findAllHistories();

        Log.v("INFO", String.valueOf(histories.size()));

        // validate the result
        if(currentHistory == null ){
            Log.v("INFO", "OPPS, history not found");
            // hemm, we're going to create one for this
            // define the model
            History historyInput = new History();
            historyInput.status = "idle";
            historyInput.currentDate = new Date();

            // start to save input
            AppDatabase.getInstance(context).historyRepo().insertAll(historyInput);
            currentHistory = AppDatabase.getInstance(context).historyRepo().findTheNewDailyHistory();
            Log.v("INFO", "ID want to create is" + historyInput.id);
        }


        // return the history
        return currentHistory;
    }

    /**
     * ## checkIn
     *
     *
     * checked the user in to the company
     *
     * @param history history data that has been updated
     * @param context context of application running activity
     */
    public void checkIn(History history, Context context){
        // start save data and update it
        AppDatabase.getInstance(context).historyRepo().updateAll(history);
    }

    /**
     * ## checkOut
     *
     *
     * checked the user in to the company
     *
     * @param history history data that has been updated
     * @param context context of application running activity
     */
    public void checkOut(History history, Context context){
        // start save data and update it
        AppDatabase.getInstance(context).historyRepo().updateAll(history);
    }


    public List<History> findAllHistoryData(Context context){
        return AppDatabase.getInstance(context).historyRepo().findAllHistories();
    }
}
