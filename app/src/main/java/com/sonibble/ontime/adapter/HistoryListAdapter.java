package com.sonibble.ontime.adapter;

import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.sonibble.ontime.R;
import com.sonibble.ontime.model.History;

import java.util.List;

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.ViewHolder> {
    private List<History> historyList;
    private Context context;
    private final SimpleDateFormat dateFormater = new SimpleDateFormat("MMMM, dd yyyy");

    public HistoryListAdapter(List<History> historyList, Context context){
        this.historyList = historyList;
        this.context = context;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView dateText, statusText;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            // define the variables
            dateText = itemView.findViewById(R.id.dateText);
            statusText = itemView.findViewById(R.id.statusText);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_history_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final History data = historyList.get(position);
//        holder.dateText.setText(dateFormater.format(data.currentDate));
        holder.statusText.setText(data.status);
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }



}
