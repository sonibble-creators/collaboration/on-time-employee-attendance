package com.sonibble.ontime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sonibble.ontime.model.User;
import com.sonibble.ontime.service.AccountService;
import com.sonibble.ontime.utils.AppDatabase;

public class CreateAccountActivity extends AppCompatActivity {
    private EditText inputFullName;
    private EditText inputEmail;
    private EditText inputPassword;
    private Button createAccountButton;
    private TextView logInLink;

    private final AccountService accountService = new AccountService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        // define the widget variables
        inputFullName = findViewById(R.id.inputFullName);
        inputEmail = findViewById(R.id.inputEmail);
        inputPassword = findViewById(R.id.inputPassword);
        createAccountButton = findViewById(R.id.createAccountButton);
        logInLink = findViewById(R.id.logInLink);

        // when the login link clicked
        logInLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bringToLogIn();
            }
        });

        // when the create Account clicked
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewAccountUser();
            }
        });
    }

    /**
     * ## createNewAccountUser
     *
     * create a new account for user
     */
    private void createNewAccountUser(){
        // define all data from the form
        final String fullName = inputFullName.getText().toString();
        final String email = inputEmail.getText().toString();
        final String password = inputPassword.getText().toString();

        // start create the account
        accountService.createAccount(fullName, email, password, getApplicationContext());

        // bring the user into the dashboard
        final Intent dashboardIntent = new Intent(CreateAccountActivity.this, DashboardActivity.class);
        startActivity(dashboardIntent);
        finish();
    }

    /**
     * ## bringToLogIn
     *
     * bring the current user to login
     */
    private void bringToLogIn(){
        // define the intent
        final Intent logInIntent = new Intent(CreateAccountActivity.this, LogInActivity.class);
        startActivity(logInIntent);
        finish();
    }

}