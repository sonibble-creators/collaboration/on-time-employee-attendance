package com.sonibble.ontime;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sonibble.ontime.model.User;
import com.sonibble.ontime.service.AccountService;
import com.sonibble.ontime.service.StorageService;

public class AccountFragment extends Fragment {
    private TextView fullName, username, bio, address;
    private ImageView avatar;
    private Button editProfileButton, logOutButton;

    private final AccountService accountService = new AccountService();
    private final StorageService storageService = new StorageService();

    public AccountFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // define the view as inflater
        final View view = inflater.inflate(R.layout.fragment_account, container, false);

        // define the widget
        fullName = view.findViewById(R.id.fullName);
        username = view.findViewById(R.id.username);
        bio = view.findViewById(R.id.bio);
        address = view.findViewById(R.id.address);
        avatar = view.findViewById(R.id.avatar);
        editProfileButton = view.findViewById(R.id.editProfileButton);
        logOutButton = view.findViewById(R.id.logOutButton);

        // define the initial data
        initData();

        // when the edit profile clicked
        editProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bringToEditProfile();
            }
        });


        // when the log out button clicked
        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
            }
        });


        // return the view at least
        return view;
    }

    private void logOut(){
        // remove all data from local value
        accountService.logOut(getContext());

        // bring all user into login
        final Intent loginIntent = new Intent(getActivity(), LogInActivity.class);
        startActivity(loginIntent);
    }


    /**
     * ## initData
     *
     * define all of data
     * to the views
     */
    private void initData(){
        // get all data for current user
        final User currentUser = accountService.currentUser(getActivity().getApplicationContext());

        // define all of the value to value
        fullName.setText(currentUser.fullName);
        username.setText("@" + currentUser.username);
        bio.setText(currentUser.bio);
        address.setText(currentUser.address);

        // define the image avatar
        Bitmap avatarImage;

        // also check if avatar null or visible
        if(currentUser.avatar != null) {
            avatarImage = storageService.loadImageFromStorage(currentUser.avatar);
            avatar.setImageBitmap(avatarImage);
        }
    }




    /**
     * ## bringToEditProfile
     *
     * bring user to edit profile activity
     */
    private void bringToEditProfile(){
        // define the intent into the edit profile
        final Intent editProfileIntent = new Intent(getActivity(), EditProfileActivity.class);
        startActivity(editProfileIntent);
    }
}