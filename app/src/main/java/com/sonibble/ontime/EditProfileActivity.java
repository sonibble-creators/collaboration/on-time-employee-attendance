package com.sonibble.ontime;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.sonibble.ontime.databinding.ActivityEditProfileBinding;
import com.sonibble.ontime.model.User;
import com.sonibble.ontime.service.AccountService;
import com.sonibble.ontime.service.StorageService;

import java.io.IOException;

public class EditProfileActivity extends AppCompatActivity {
    private EditText inputEmail, inputFullName, inputUsername, inputPassword, inputAddress, inputBio;
    private ImageView avatar, backButton;
    private Button saveButton;

    private final AccountService accountService = new AccountService();
    private final StorageService storageService = new StorageService();
    private User currentUser;
    private Bitmap avatarBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        // define the all views
        inputEmail = findViewById(R.id.inputEmail);
        inputFullName = findViewById(R.id.inputFullName);
        inputUsername = findViewById(R.id.inputUsername);
        inputPassword = findViewById(R.id.inputPassword);
        inputAddress = findViewById(R.id.inputAddress);
        inputBio = findViewById(R.id.inputBio);
        saveButton = findViewById(R.id.saveButton);
        avatar =findViewById(R.id.avatar);
        backButton = findViewById(R.id.backButton);


        // init the data for profile
        initData();

        // when avatar clicked
        // update the current avatar
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });


        // when back clicked
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToDashboard();
            }
        });


        // when the save button clicked
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNewProfile();
            }
        });
    }

    /**
     * ## backToDashboard
     *
     * back the profile into the dashboard
     */
    private void backToDashboard(){
        // define the intent of dashboard
        final Intent dashboardIntent = new Intent(EditProfileActivity.this, DashboardActivity.class);
        startActivity(dashboardIntent);
        finish();
    }

    /**
     * ## pick image for avatar
     * use the activity result to get all data from the gallery
     */
    private void pickImage(){
        // define the intent to pick the image
        Intent pickImageIntent = new Intent();
        pickImageIntent.setType("image/*");
        pickImageIntent.setAction(Intent.ACTION_GET_CONTENT);

        pickImageAvatarActivityResult.launch(pickImageIntent);
    }

    //define the result activity
    // to pick image
    ActivityResultLauncher<Intent> pickImageAvatarActivityResult
            = registerForActivityResult(
            new ActivityResultContracts
                    .StartActivityForResult(),
            result -> {
                // validate the result and code
                if (result.getResultCode()
                        == Activity.RESULT_OK) {
                    // get data from intent and convert into the image url
                    Intent data = result.getData();
                    if (data != null
                            && data.getData() != null) {
                        Uri selectedImageUri = data.getData();

                        try {
                            Bitmap selectedImageBitmap
                                    = MediaStore.Images.Media.getBitmap(
                                    this.getContentResolver(),
                                    selectedImageUri);

                            // if not error found.
                            // we also want to set the image preview into the avatar
                            avatarBitmap = selectedImageBitmap;
                            avatar.setImageBitmap(selectedImageBitmap);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });


    /**
     * ## initData
     *
     * get all data that we need and define into the need
     * views
     */
    private void initData(){
        // get all data for current user
        currentUser = accountService.currentUser(getApplicationContext());

        // now set all data for input and detail
        inputEmail.setText(currentUser.email);
        inputFullName.setText(currentUser.fullName);
        inputUsername.setText(currentUser.username);
        inputPassword.setText(currentUser.password);
        inputAddress.setText(currentUser.address);
        inputBio.setText(currentUser.bio);

        // also check if avatar null or visible
        if(currentUser.avatar != null) {
            Bitmap avatarImage = storageService.loadImageFromStorage(currentUser.avatar);
            avatar.setImageBitmap(avatarImage);
        }

        // done
    }

    /**
     * ## saveNewProfile
     *
     * save the new current profile
     */
    private void saveNewProfile(){
        // start by get all data from the input
        final String email = inputEmail.getText().toString();
        final String fullName = inputFullName.getText().toString();
        final String username = inputUsername.getText().toString();
        final String password = inputPassword.getText().toString();
        final String address = inputAddress.getText().toString();
        final String bio = inputBio.getText().toString();

        // start define the model of user
        User userInput = currentUser;
        userInput.email = email;
        userInput.fullName = fullName;
        userInput.username = username;
        userInput.password = password;
        userInput.address = address;
        userInput.bio = bio;

        // save the avatar if needed
        if(avatarBitmap != null){
            // save the avatar locally
            userInput.avatar = storageService.saveImageToInternalStorage(avatarBitmap, getApplicationContext(), "profile.png");
        }

        // now start update the user
        accountService.updateUser(userInput, getApplicationContext());

        // bring back to dashboard
        final Intent dashboardIntent = new Intent(EditProfileActivity.this, DashboardActivity.class);
        startActivity(dashboardIntent);
        finish();
    }

}