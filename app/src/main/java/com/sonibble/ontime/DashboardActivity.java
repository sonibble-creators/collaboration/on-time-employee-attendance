package com.sonibble.ontime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class DashboardActivity extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener {
    private BottomNavigationView bottomNavigationBar;

    // define the fragment
    // used in the menu navigation bar
    private final HomeFragment homeFragment = new HomeFragment();
    private final HistoryFragment historyFragment = new HistoryFragment();
    private final AccountFragment accountFragment = new AccountFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // define the widget view
        bottomNavigationBar = findViewById(R.id.bottonNavigatorBar);

        // define the bottom bar listener and default menu selected
        bottomNavigationBar.setOnItemSelectedListener(this);
        bottomNavigationBar.setSelectedItemId(R.id.homeMenu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // checking and update the menu of bottom bar
        switch (item.getItemId()){
            case R.id.homeMenu:
                getSupportFragmentManager().beginTransaction().replace(R.id.pageViewNavigation, homeFragment).commit();
                return true;

            case R.id.historyMenu:
                getSupportFragmentManager().beginTransaction().replace(R.id.pageViewNavigation, historyFragment).commit();
                return true;

            case R.id.accountMenu:
                getSupportFragmentManager().beginTransaction().replace(R.id.pageViewNavigation, accountFragment).commit();
                return true;
        }

        // return default
        return false;
    }
}