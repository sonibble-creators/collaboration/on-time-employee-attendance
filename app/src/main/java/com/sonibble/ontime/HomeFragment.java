package com.sonibble.ontime;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.sonibble.ontime.model.History;
import com.sonibble.ontime.service.HistoryService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class HomeFragment extends Fragment implements LocationListener {
    // define the variables
    private Button checkOutButton, checkInButton;
    private TextView statusHistory, timeText, dateText;
    private LocationManager locationManager;
    private ImageView mapsView;
    private Location currentLocation;
    private History currentHistory;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM, dd yyyy");
    private final SimpleDateFormat timFormat = new SimpleDateFormat("HH:mm");
    private final Handler timeHandler = new Handler();
    private final HistoryService historyService = new HistoryService();

    // predefined company location
    private final Location companyLocation = new Location("STIKOM BALI");
    private final float MAX_RADIUS_LOCATION = 500;


    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_home, container, false);

        // define the vars
        checkInButton = view.findViewById(R.id.checkInButton);
        checkOutButton = view.findViewById(R.id.checkOutButton);
        statusHistory = view.findViewById(R.id.statusHistory);
        timeText = view.findViewById(R.id.timeText);
        dateText = view.findViewById(R.id.dateText);
        mapsView = view.findViewById(R.id.mapsView);

        // define the location of company
        // this location refer to STIKOM BALI
        companyLocation.setLatitude(-8.6732);
        companyLocation.setLongitude(115.2266);

        // define location manager
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        // init data
        initData();

        // start init the location
        initLocation();

        // when checkin clicked
        checkInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInNow();
            }
        });

        // when checkout clicked
        checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkOutNow();
            }
        });

        // return the view
        return view;
    }

    /**
     * checkInNow
     *
     * check the user in teh company
     * by change the status and validate the location circle
     */
    private void checkInNow(){
        // ensure the location is nearby the company
        // get the radius
        float[] distanceGap = new float[2];
        Location.distanceBetween(companyLocation.getLatitude(), companyLocation.getLongitude(), currentLocation.getLatitude(), currentLocation.getLongitude(), distanceGap);

        if(distanceGap[0] < MAX_RADIUS_LOCATION){
            // nice location inside the circle
            History historyInput = currentHistory;
            historyInput.status = "Checked In";
            historyInput.checkInAt = new Date();

            // start save data
            // and update data
            historyService.checkIn(historyInput, getContext());
            currentHistory = historyInput;
            initData();

            // also show the alert succes
            showAlertSuccess();
        }else{
            // opps, location outside the circle
            showAlertFailed();
        }
    }

    /**
     * checkOutNow
     *
     * check the user in teh company
     * by change the status and validate the location circle
     */
    private void checkOutNow(){
        // ensure the location is nearby the company
        // get the radius
        float[] distanceGap = new float[2];
        Location.distanceBetween(companyLocation.getLatitude(), companyLocation.getLongitude(), currentLocation.getLatitude(), currentLocation.getLongitude(), distanceGap);

        if(distanceGap[0] < MAX_RADIUS_LOCATION){
            // nice location inside the circle
            History historyInput = currentHistory;
            historyInput.status = "Checked Out";
            historyInput.checkoutAt = new Date();

            // start save data
            // and update data
            historyService.checkOut(historyInput, getContext());
            currentHistory = historyInput;
            initData();

            // also show the alert succes
            showAlertSuccess();
        }else{
            // opps, location outside the circle
            showAlertFailed();
        }
    }


    // dialog
    private void showAlertFailed(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle("Failed");
        dialogBuilder.setMessage("Opps, cannot check in, please ensure you're in the location");
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    private void showAlertSuccess(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle("Success");
        dialogBuilder.setMessage("Hurray, you're awesome. Enjoy now");
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    // init data
    private void initData(){
        // define date for now
        final Date dateTimeNow = new Date();
        final String currentDateString = dateFormat.format(dateTimeNow);


        // start set data to view
        dateText.setText(currentDateString);
        mapsView.setImageResource(R.drawable.map_view);

        // start tick the time
        timeHandler.post(startTickTime());

        // find the current history first and init it
        currentHistory = historyService.initHistoryDaily(getContext());


        // validate now
        // and define default text view
        if(currentHistory != null){
            statusHistory.setText(currentHistory.status);

            // hide button when the already checkin or checkout
            if(currentHistory.checkInAt == null && currentHistory.checkoutAt == null){
                checkInButton.setVisibility(View.VISIBLE);
                checkOutButton.setVisibility(View.GONE);
            }

            if(currentHistory.checkInAt != null){
                checkInButton.setVisibility(View.GONE);
                checkOutButton.setVisibility(View.VISIBLE);
            }

            if(currentHistory.checkoutAt != null && currentHistory.checkInAt != null){
                checkOutButton.setVisibility(View.GONE);
                checkInButton.setVisibility(View.GONE);
            }
        }

    }

    // start to update time periodically
    private Runnable startTickTime(){
        return new Runnable(){
            @Override
            public void run() {
                // update the text
                final String stringTime = timFormat.format(new Date());
                timeText.setText(stringTime);

                // then call again this function
                timeHandler.postDelayed(startTickTime(), (1000 * 60));
            }
        };
    }

    // init the location
    // including granted permision
    private void initLocation(){
        // check the permision of app
        // if permision not granted, request em
        final Boolean isGpsEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        final Boolean isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if( isGpsEnable  && isNetworkEnable){
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }

            // first define the last location of user
            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            // define the location listener
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        currentLocation = location;
    }
}