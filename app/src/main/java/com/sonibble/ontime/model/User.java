package com.sonibble.ontime.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class User {
    @PrimaryKey(autoGenerate = false)
    public Integer id;

    @ColumnInfo(name = "avatar")
    public String avatar;

    @ColumnInfo(name = "email")
    public String email;

    @ColumnInfo(name = "username")
    public String username;

    @ColumnInfo(name = "full_name")
    public String fullName;

    @ColumnInfo(name = "password")
    public String password;

    @ColumnInfo(name = "address")
    public String address;

    @ColumnInfo(name = "bio")
    public String bio;
}
