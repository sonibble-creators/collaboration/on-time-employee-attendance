package com.sonibble.ontime.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.LocalDateTime;
import java.util.Date;

@Entity(tableName = "history")
public class History {
    @PrimaryKey(autoGenerate = true)
    public Integer id;

    @ColumnInfo(name = "status")
    public String status;

    @ColumnInfo(name = "current_date")
    public Date currentDate;

    @ColumnInfo(name = "check_in_at")
    public Date checkInAt;

    @ColumnInfo(name = "check_out_at")
    public Date checkoutAt;
}