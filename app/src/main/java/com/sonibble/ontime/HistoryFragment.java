package com.sonibble.ontime;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sonibble.ontime.adapter.HistoryListAdapter;
import com.sonibble.ontime.model.History;
import com.sonibble.ontime.service.HistoryService;

import java.util.ArrayList;
import java.util.List;

public class HistoryFragment extends Fragment {
    private RecyclerView listViewHistory;
    private HistoryListAdapter listHistoryAdapter;
    private RecyclerView.LayoutManager listHistoryLayoutManager;
    private List<History> historyList = new ArrayList<>();

    private final HistoryService historyService = new HistoryService();


    public HistoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_history, container, false);

        // define all data
        listViewHistory = view.findViewById(R.id.listViewHistory);
        listHistoryLayoutManager = new LinearLayoutManager(getContext());
        listViewHistory.setLayoutManager(listHistoryLayoutManager);
        listHistoryAdapter = new HistoryListAdapter(historyList, getContext());
        listViewHistory.setAdapter(listHistoryAdapter);

        // start define data
        initData();

        // return it
        return view;
    }



    private void initData(){
        // get all data
        // including the history and update it
        historyList = historyService.findAllHistoryData(getContext());
        Log.v("INFO", "TOTAL DATA IS"+ String.valueOf(historyList.size()));
        listHistoryAdapter.notifyDataSetChanged();

    }
}